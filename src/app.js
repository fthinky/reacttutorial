
import React from 'react';
import ReactDOM from 'react-dom';
import { IndexUserList } from './userList'
import { Home } from './home'
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import IndexRedux from './userListRedux'
import IndexFetch from './fetchIslemleri'
import FetchImage from './fetchIslemleri/fetchImage'
export class App extends React.Component {
  aaa = () => {
    return (<div>fatih uysal</div>)
  }
  Home = () => {
    return (
      <div>
        <h2>Home</h2>
      </div>
    );
  }
  render() {
    return <div>
      Merhaba
      <Router>       
       <div>
         AAAA
          <ul>
            <li>
              <Link to="/">Home</Link>
            </li>
            <li >
              <Link to="/ListeleWithoutRedux">Kullancı Listeleme - Redux olmadan</Link>

            </li>
            <li> <Link to="/ListeleWithRedux">Kullanıcı Listeleme - Redux</Link></li>
            <li> <Link to="/FetchIslemleri">Fetch Kullanımı</Link></li>
            <li> <Link to="/FetchIslemleri2">Fetch Kullanımı2</Link></li>
          </ul>
          <hr />
          <Route exact path="/" component={Home} />
          <Route path="/ListeleWithoutRedux" component={IndexUserList} />
          <Route path="/ListeleWithRedux" component={IndexRedux} />
          <Route path="/FetchIslemleri" component={IndexFetch} />
          <Route path="/FetchIslemleri2" component={FetchImage } />
        </div>
      </Router>
    </div>;
  }
}
ReactDOM.render(<App />, document.getElementById("container"));

