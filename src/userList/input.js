import React from 'react';
import PropTypes from 'prop-types';
export class Input extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            input: {
                name: "",
                email: ""
            }
        }
    }



    handleClickKaydet = (e) => {
        e.stopPropagation();
        e.preventDefault();
        this.setState({
            input: {
                name: this.inputName.value,
                email: this.inputEmail.value
            }
        }
            ,
            () => {
                this.props.onClickKaydet(this.state.input);
                this.clearInputs();
            }
        );

    }
    handleClickIptal = (e) => {
        this.clearInputs();
        this.props.onClickIptal();
    }
    // handleChangeName = (e) => {
    //     this.setState({ input: { name: e.target.value } });
    // }
    // handleChangeEmail = (e) => {
    //     this.setState({ input: { email: e.target.value } });
    // }
    clearInputs = () => {
        this.inputName.value = "";
        this.inputEmail.value = "";
    }

    componentDidMount() {
        // alert("componentDidMount");
    }
    componentDidUpdate(prevProps, prevState, snapshot) {
       
        if (this.props.btnStatu === 2) {
            this.inputName.value = this.props.editedUser.name;
            this.inputEmail.value = this.props.editedUser.email;
        }
    }
    componentWillUnmount() {
        // alert("componentWillUnmount");
    }
    // shouldComponentUpdate(nextProps, nextState){
    //     alert("shouldComponentUpdate");
    // }
    renderSwitch() {
        switch (this.props.btnStatu) {
            case 1:
                return <h2>Yeni Kayıt</h2>;
            case 2:
                return <h2>Kayıt Düzenle</h2>;
        }
    }
    render() {
        return <div style={{ display: this.props.btnStatu > 0 ? "block" : "none" }}>

            {this.renderSwitch()}

            {/* <div className="form-group row">
                <div className="col-md-6">
                    <label htmlFor="exampleInputId">Id</label>
                </div>
                <div className="col-md-6">
                    {this.state.input.id}
                </div>
            </div> */}
            <div className="form-group row">
                <div className="col-md-6">
                    <label htmlFor="exampleInputName1">Name</label>
                </div>
                <div className="col-md-6">
                    <input disabled={this.props.btnStatu ===2? true:false} type="text" className="form-control" id="exampleInputName1" placeholder="Jane Doe" ref={r => (this.inputName = r)} />
                </div>
            </div>

            <div className="fom-group row">
                <div className="col-md-6">
                    <label htmlFor="exampleInputEmail1">Email</label>
                </div>
                <div className="col-md-6">
                    <input type="email" className="form-control" required id="exampleInputEmail1" placeholder="jane.doe@example.com" aria-describedby="emailHelp" ref={r => (this.inputEmail = r)} />
                    <small id="emailHelp" className="form-text text-muted">test1</small>
                </div>
            </div>
            <div className="fom-group row">
                <button className="btn btn-default" onClick={this.handleClickKaydet}>Kaydet</button>
                <button className="btn btn-default" onClick={this.handleClickIptal}>İptal</button>
            </div>
            <br />
            <br />
            <div>{this.state.input.name}</div>
            <div>{this.state.input.email}</div>
            <div id="sucessMessage" className="alert alert-success" style={{ display: this.props.IsSuccess ? 'block' : 'none' }}>
                <strong>Başarılı!</strong> işlem kaydedildi.
            </div>

            {this.props.IsSuccess === false &&
                <div id="sucessMessage2" className="alert alert-danger">
                    <strong>Başarısız!</strong> işlem gerçekleştirelemedi.
          </div>
            }
        </div>;
    }
}



Input.propTypes = {
    IsSuccess: PropTypes.bool,
    onClick: PropTypes.func
}



