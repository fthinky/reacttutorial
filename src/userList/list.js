import React from "react"
import PropTypes from 'prop-types';
export class List extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            userList: this.props.userList
        }
    }

    handleClikNew = () => {
        this.props.onClickNew();
    }
    handleClickSearch=()=>{
       var newUserList= this.props.userList.filter((user,index)=>{
            return user.name.includes(this.inputSearch.value);

        })
        this.setState({ userList: newUserList });
    }

    handleEditInTable = (user, rowId) => {
        this.props.onClickEditInTable(user, rowId);
    }
    handleDeleteInTable = (name) => {
        this.props.onClickDeleteInTable(name);
    }
    componentWillReceiveProps(nextProps) {
        if (this.props.userList !== nextProps.userList) {
            this.setState({ userList: nextProps.userList });
        }
    }

    renderList = () => {
        return this.state.userList.map((user, index) => {
            const email = `${user.email} - ${index}`;
            const email2 = user.email + "-" + index;
            const rowId = index + 1;
            return (
                <tr>
                    <td>{rowId}</td>
                    <td>{user.name}</td>
                    <td>{email}</td>
                    <td>
                        <button className="btn btn-link" onClick={() => this.handleDeleteInTable(user.name)}>
                            Sil
                       </button>
                        <button className="btn btn-link" onClick={() => this.handleEditInTable(user, rowId)}>Düzenle</button>
                    </td>
                </tr>
            );
        });
    }
    render() {
        return <div>
            {this.props.btnStatu === 0 &&
                <button className="btn btn-default" onClick={this.handleClikNew}>Yeni Kayıt</button>}
            <br />
            <br />
            <div className="form-group row">
                <div className="col-md-12">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Search for..." ref={r => (this.inputSearch = r)}/>
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="button" onClick={this.handleClickSearch}>Go!</button>
                        </span>
                    </div>
                </div>


            </div>
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Name</th>
                        <th scope="col">Email</th>
                        <th scope="col">Edit</th>
                    </tr>
                </thead>
                <tbody>
                    {this.renderList()}
                </tbody>
            </table>
        </div>
    }
}