import React from 'react';
import ReactDOM from 'react-dom';
import { Input } from './input'
import { List } from './list'
export class IndexUserList extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            IsSuccess: undefined,
            btnStatu: 0,
            editedUser: {
                name: "",
                email: ""
            },
            userList: [
                {
                    name: "fatih",
                    email: "fatih@gmail.com"
                },
                {
                    name: "mehmet",
                    email: "mehmet@gmail.com"
                }
            ]
        }
    }

    handleClickKaydet = (input) => {
        this.setState({
            editedUser: {
                name: input.name,
                email: input.email
            }
        })
        if (this.state.btnStatu === 1) {
            var result = this.state.userList.filter(user => {
                return user.name === input.name
            })
            if (result.length === 0) {
                this.setState({
                    userList: [...this.state.userList, input]
                }
                    , () => {
                        this.setState({
                            IsSuccess: true
                        });
                    }
                );
            }
            else {
                this.setState({
                    IsSuccess: false
                });
            }
        }
        else if (this.state.btnStatu === 2) {
            
            var newUserList =this.state.userList.map((user, index) => {
                if (user.name === input.name) {
                    return input;
                }
                else{
                    return user;
                }
            })

            this.setState({
                userList: newUserList
            });

        }

    };
    handleClickIptal = () => {
        this.setState({
            btnStatu: 0
        })
    };
    hadleClickNew = () => {
        this.setState({
            btnStatu: 1
        })
    }
    handleEditInTable = (user, rowId) => {
        this.setState({
            btnStatu: 2,
            editedUser: {
                name: user.name,
                email: user.email
            }
        })
    }
    handleDeleteInTable = (name) => {
        this.setState({
            btnStatu: 0
        })
    }

    render() {
        return <div style={{ marginTop: '15px' }}>
            <Input onClickKaydet={this.handleClickKaydet} onClickIptal={this.handleClickIptal} IsSuccess={this.state.IsSuccess} btnStatu={this.state.btnStatu} editedUser={this.state.editedUser} />
            <List onClickNew={this.hadleClickNew} onClickEditInTable={this.handleEditInTable} onClickDeleteInTable={this.handleDeleteInTable} btnStatu={this.state.btnStatu} userList={this.state.userList} />
        </div>;
    }
}
// ReactDOM.render(<Index />, document.getElementById("container"));

