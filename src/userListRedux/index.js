4
import React, { Component } from 'react'
import store  from './redux/store'
import { Provider} from 'react-redux';
import Input from './input';
export default class IndexRedux extends Component {
  render() {
    return (
      <div>
        <Provider store={store}>
        <Input/>
        </Provider>
      </div>
    )
  }
}
