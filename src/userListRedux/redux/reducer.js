import * as PageActionTypes from './actionTypes'
import { combineReducers } from 'redux'
const initialState = {
    sayi: 0,
    input: {
      name: "fatih",
      email: "fatih@gmail.com"
  },
  btnStt:1,
  isSuccess:undefined
}
function addReducer(state = initialState, action) {
    
    switch (action.type) {
      case PageActionTypes.SET_SAYI:
      return Object.assign({}, state, {sayi:state.sayi+ action.options})
      case PageActionTypes.SET_INPUT_OBJ:
      return Object.assign({}, state, {input: action.options})
      case PageActionTypes.SET_BTN_STT:
      return Object.assign({}, state, {btnStt: action.options})
      case PageActionTypes.SET_IS_SUCCESS:
      return Object.assign({}, state, {IsSuccess: action.options})
      default:
        return state;
    }
    
  }

// const reducerCombined= combineReducers({
//     addReducer
//   });

  export default addReducer;