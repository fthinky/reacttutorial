import * as PageActionTypes from "./actionTypes";
// import { SET_SAYI } from './actionTypes'

export function setSayiState(options) {
  return {
    type: PageActionTypes.SET_SAYI,
    options: options
  };
}

export function setInputObj(options) {
  
  return {
    type: PageActionTypes.SET_INPUT_OBJ,
    options: options
  };
}

export function setBtnStt(options) {
  return {
    type: PageActionTypes.SET_BTN_STT,
    options: options
  };
}
export function setIsSuccess(options) {
  return {
    type: PageActionTypes.SET_IS_SUCCESS,
    options: options
  };
}