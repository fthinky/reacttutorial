import React, { Component } from 'react'
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import * as PageActions from "./redux/actions";
class Input extends Component {
  constructor(props) {
    super(props);

  }

  arttir = sayi => {
    this.props.actions.setSayiState(sayi);

  };
  handleClickKaydet = (e) => {
    e.stopPropagation();
    e.preventDefault();
    this.props.actions.setInputObj({
        name: this.inputName.value,
        email: this.inputEmail.value
    }
      ,
      () => {
        this.clearInputs();
      }
    );

  }
  clearInputs = () => {
    this.inputName.value = "";
    this.inputEmail.value = "";
  }

  componentDidUpdate(prevProps, prevState, snapshot) {

    if (this.props.state.btnStt === 2) {
      this.inputName.value = this.props.editedUser.name;
      this.inputEmail.value = this.props.editedUser.email;
    }
  }
  handleClickIptal = (e) => {
    this.clearInputs();
   // this.props.onClickIptal();todo
  }
  renderSwitch() {
    switch (this.props.state.btnStt) {
      case 1:
        return <h2>Yeni Kayıt</h2>;
      case 2:
        return <h2>Kayıt Düzenle</h2>;
    }
  }
  render() {
    return (
      <div style={{ display: this.props.state.btnStt > 0 ? "block" : "none" }}>
        {this.renderSwitch()}
        <div className="form-group row">
          <div className="col-md-6">
            <label htmlFor="exampleInputName1">Name</label>
          </div>
          <div className="col-md-6">
            <input disabled={this.props.state.btnStt === 2 ? true : false} type="text" className="form-control" id="exampleInputName1" placeholder="Jane Doe" ref={r => (this.inputName = r)} />
          </div>
        </div>

        <div className="fom-group row">
          <div className="col-md-6">
            <label htmlFor="exampleInputEmail1">Email</label>
          </div>
          <div className="col-md-6">
            <input type="email" className="form-control" required id="exampleInputEmail1" placeholder="jane.doe@example.com" aria-describedby="emailHelp" ref={r => (this.inputEmail = r)} />
            <small id="emailHelp" className="form-text text-muted">test1</small>
          </div>
        </div>
        <div className="fom-group row">
          <button className="btn btn-default" onClick={this.handleClickKaydet}>Kaydet</button>
          <button className="btn btn-default" onClick={this.handleClickIptal}>İptal</button>
        </div>
        <br />
        <br />
        <div>{this.props.state.input.name}</div>
        <div>{this.props.state.input.email}</div>
        <div id="sucessMessage" className="alert alert-success" style={{ display: this.props.state.isSuccess ? 'block' : 'none' }}>
          <strong>Başarılı!</strong> işlem kaydedildi.
        </div>

        {this.props.state.isSuccess === false &&
          <div id="sucessMessage2" className="alert alert-danger">
            <strong>Başarısız!</strong> işlem gerçekleştirelemedi.
          </div>
        }
      </div>

      // <div>
      //   Sayfa2
      //   <h1>{this.props.state.sayi}</h1>
      //   <button onClick={event => this.arttir(1)}>increment</button>
      // </div>
    );
  }
}
const mapStateToProps = state => ({

  state: state

});
const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(PageActions, dispatch)
});
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Input);