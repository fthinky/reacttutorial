import React, { Component } from 'react'

export default class FetchImage extends Component {
    constructor(props){
        super(props);
        this.state = {
            selectValue: "1",
            isLoading: true,
            photos: [],
            error: null,
            imgUrl: "",
            imgTitle: ""
          };
    }
    componentDidMount() {
      this.fetchPhotos();
    }
    handleAra = event => {
      debugger;
      alert("Your favorite flavor is: " + this.state.selectValue);
      event.preventDefault();
    };
  
    handleChange = event => {
      this.setState({ selectValue: event.target.value });
    };
    handleImageClik = photo => {
      this.setState({ imgUrl: photo.url, imgTitle: photo.title });
    };
  
    fetchPhotos() {
      console.time();
  
      // Where we're fetching data from
      fetch(`https://jsonplaceholder.typicode.com/photos`)
        // We get the API response and receive data in JSON format...
        .then(response => response.json())
        // ...then we update the users state
        .then(data =>
          this.setState({
            photos: data,
            isLoading: false
          })
        )
        // Catch any errors we hit and update the app
        .catch(error => this.setState({ error, isLoading: false }));
      console.timeEnd();
    }
    renderList = () => {
      let albumList = this.state.photos.filter((photo, index) => {
        return photo.albumId.toString() === this.state.selectValue;
      });
      return albumList.map(photo => {
        return (
          <img
            src={photo.thumbnailUrl}
            alt="Smiley face"
            height="150"
            width="150"
            style={{ padding: "5px" }}
            onClick={event => this.handleImageClik(photo)}
          />
        );
      });
    };
    render() {
      return (
        <div className="">
          {this.state.isLoading ? (
            <div>Sayfa Yükleniyor</div>
          ) : (
            <div className="left" style={{ maxWidth: "50%" }}>
              <div>
                <select
                  name="form-field-name"
                  value={this.state.selectValue}
                  onChange={this.handleChange}
                >
                  <option value="1">1</option>
                  <option value="2">2</option>
                  <option value="3">3</option>
                  <option value="4">4</option>
                </select>
  
                <button onClick={event => this.handleAra(event)}>Ara</button>
              </div>
              <br />
              <br />
              <div
                style={{
                  maxHeight: "350px",
                  overflow: "hidden",
                  overflowY: "auto"
                }}
              >
                {this.renderList()}
              </div>
            </div>
          )}
          <div className="left" style={{ paddingLeft: "20px", maxWidth: "50%" }}>
            {this.state.imgUrl.length > 0 && (
              <div>
                <div>
                  <img
                    src={this.state.imgUrl}
                    alt="Smiley face"
                    height="600"
                    width="600"
                    style={{ padding: "5px" }}
                  />
                </div>
                <div>
                  <span>{this.state.imgTitle}</span>
                </div>
              </div>
            )}
          </div>
        </div>
      );
    }
  }
  