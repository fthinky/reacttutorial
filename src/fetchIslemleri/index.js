import React, { Component } from 'react'

export default class Index extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      itemList: [],
      selectedItem: { id: "1", title: "" }
    };
  }

  componentDidMount() {
    fetch('https://jsonplaceholder.typicode.com/todos')
      .then(response => response.json())
      .then(json =>
        json.map((data, index) => {
          var item = {
            id: data.id,
            title: data.title
          }
          this.setState({
            itemList: [...this.state.itemList, item],
            isLoading: false
          })
        })
      )
  }
  handleAra = (event) => {
    debugger;
    fetch('https://jsonplaceholder.typicode.com/todos/' + this.state.selectedItem.id)
      .then(response => response.json())
      .then(json =>
        this.setState({
          selectedItem: {
            title: json.title
          }
        })
      )
  }
  handleChange = event => {
    this.setState({
      selectedItem: {
        id: event.target.value,
        title: ""
      }
    })
  };
  render() {

    return (
      <div className="">
        {this.state.isLoading ? (
          <div>Sayfa Yükleniyor</div>
        ) : (<div>
          <div>
            <select value={this.state.selectedItem.id} onChange={this.handleChange}>
              {this.state.itemList.map((item, index) => {
                return <option value={item.id}>{item.title}</option>;
              })}
            </select>
            <button onClick={event => this.handleAra(event)}>Ara</button>
          </div>
          <div>
            {this.state.selectedItem.title}
          </div>
        </div>
          )}

      </div>
    );


  }
}
